package ua.danIt.framework;

import static ua.danIt.framework.LoginServlet.yes;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.danIt.Application.dao.JdbcUserDao;
import ua.danIt.Application.model.User;

/**
 * Created  12.02.2018.
 */
public class SelectedUsersServlet extends HttpServlet{

    JdbcUserDao userDao = new JdbcUserDao();
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
  if (yes) {
    PrintWriter out = resp.getWriter();
    out.write(ChatRoomServlet.head);
    out.write("<body>");

    for (User item : userDao.getSelectedUsers()) {

      out.write("<form action='' method='POST'>");
      out.write("<button class=\"btn col-sm-offset-4 col-sm-4  btn-primary \" type = 'submit'>");
      out.write(item.NAME);
      out.write("</button>");
      out.write("<input type=\"hidden\" NAME='ID'value=\"" + item.ID + "\">");
      out.write("</form>");

    }

    out.write("</body></html>");
  } else {
    resp.sendRedirect("/login");
  }
  }
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.sendRedirect("/message/"+req.getParameter("ID"));
  }
}
