package ua.danIt.framework;

import static ua.danIt.framework.LoginServlet.yes;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.danIt.Application.dao.JdbcUserDao;
import ua.danIt.Application.model.User;

/**
 * Created  12.02.2018.
 */
public class ChoiseUserServlet extends HttpServlet {

  JdbcUserDao userDao = new JdbcUserDao();

  String head = "<!doctype html>\n" +
      "<html lang=\"en\" xmlns:margin-bottom=\"http://www.w3.org/1999/xhtml\">\n" +
      "<head>\n" +
      "    <meta charset=\"UTF-8\">\n" +
      "    <meta NAME=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
      "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
      "    <title>Document</title>\n" +
      "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n" +
      "    <style>\n" +
      "        img{\n" +
      "            width: 600px;\n" +
      "            height: 400px;\n" +
      "            border-radius: 50%;\n" +
      "            margin-bottom: 15px;\n" +
      "        }\n" +
      "        h1{\n" +
      "            margin-top:0px;\n" +
      "        }\n" +
      "    </style>\n" +
      "</head>\n" +
      "<body>\n" +
      "<div class=\"container\">\n" +
      "    <div class=\"row\" >\n" +
      "        <div class=\"col-sm-12 MSG_TEXT-center\">";

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    if (yes) {
      User user = userDao.getCurrentUser();
      String url = user.URL;
      String name = user.NAME;
      int id = user.ID;
      PrintWriter out = resp.getWriter();
      out.write(head);
      out.write("<div class=\"col-sm-12 text-center\">");
      out.write("<img  src=" + url + ">");
      out.write("</div>\n" +
          "            <div class=\" col-sm-12\" >\n" +
          "                <form action='/' method='POST'>\n" +
          "                    <button  class=\"btn col-sm-4 btn-primary\" type=\"submit\">YES</button>");
      out.write("<h1 class=\"col-sm-4 text-center\">" + name + "</h1>");
      out.write("<button class=\"btn col-offset-1 col-sm-4 btn-success\" >NO</button>");
      out.write("<input type=\"hidden\" NAME='nameChoice' value=" + id + ">");
      out.write(" </form>\n" +
          "            </div>\n" +
          "    </div>\n" +
          "</div>\n" +
          "</body>\n" +
          "</html>");
    } else {
      resp.sendRedirect("/login");
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      String input = req.getParameter("nameChoice");
      int userId = Integer.parseInt(input);
    try {
      userDao.selectUser(userId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    resp.sendRedirect("/");
    }
}
