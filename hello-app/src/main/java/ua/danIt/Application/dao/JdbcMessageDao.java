package ua.danIt.Application.dao;

import static java.sql.DriverManager.getConnection;
import static ua.danIt.framework.LoginServlet.curID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import ua.danIt.Application.model.Message;
import ua.danIt.framework.utils.EntityUtils;

public class JdbcMessageDao {
  String PASSWD = "paRis";
  String DB_URL = "jdbc:mysql://danit.cukm9c6zpjo8.us-west-2.rds.amazonaws.com:3306/fs1";
  String USER = "fs1_user";
  public void save(Message msg) throws SQLException {
    String sql = "INSERT INTO `MESSAGES`" +
        "(FROM_USER,TO_USER,MSG_TEXT, TIMESTAMP) " +
        "VALUES (?,?,?,?)";
    try(Connection con = getConnection(DB_URL, USER, PASSWD);
        PreparedStatement ps = con.prepareStatement
            (sql)) {
      ps.setInt(1, msg.FROM_USER);
      ps.setInt(2, msg.TO_USER);
      ps.setString(3, msg.MSG_TEXT);
      ps.setLong(4, msg.TIMESTAMP);
      ps.executeUpdate();
    }
  }

    public List<Message> showUsers(int id){
      String sql = "select * from fs1.MESSAGES where (FROM_USER='"+curID+"' and TO_USER='"+id+"') or (FROM_USER='"+id+"' and TO_USER='"+curID+"')";
      return EntityUtils.nativeQuery(sql,Message.class);
    }
}
